#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#define MEMSIZE 4096
enum { false, true };

typedef void (*Instruction_Table)(struct Machine *chip8);
struct State
{
	uint16_t opc, nnn;
	uint8_t kk, n, x ,y, p;
};

struct Machine
{
	uint8_t ram[MEMSIZE];	// Memoria principal
	uint16_t pc;			// PC

	uint8_t sp;				// SP
	uint16_t stack[16];		// Pila

	uint8_t v[16];			// Banco de registros, posición 0xF es el carry
	uint16_t i;				// Registro especial "I"

	uint8_t delay, sound;	//Temporizadores "delay" y "sound"

	struct State state;
};

void init(struct Machine *chip8)
{
	chip8->sp = chip8->i = chip8->delay = chip8->sound = 0x00;
	chip8->pc = 0x200;

	memset(chip8->ram, 0x00, MEMSIZE);
	memset(chip8->stack, 0x00, 16);
	memset(chip8->v, 0x00, 16);
}

void load_rom(struct Machine *chip8, const char *rom_file)
{
	FILE *pf = fopen(rom_file, "r");
	if(NULL != pf)
	{
		fseek(pf, 0, SEEK_END);
		int tam = ftell(pf);
		fseek(pf, 0, SEEK_SET);
		fread(chip8->ram + 0x200, tam, 1, pf);
		fclose(pf);
	}
	else
	{
		printf("El fichero indicado no puede abrirse.\n");
		exit(1);
	}
}

void p0 (struct Machine *chip8 )
{
	if(0x00E0 == chip8->state.opc)
	{
		//TODO
		printf("CLS\n");
	}
	else if(0x00EE == chip8->state.opc)
	{
		if(chip8->sp > 0)
		{
			chip8->pc = chip8->stack[chip8->sp];
			chip8->sp--;
		}
	}
	else
	{
		//printf("SYS %x\n", nnn);
	}
}

void p1 (struct Machine *chip8 )
{
	chip8->pc = chip8->state.nnn;
}

void p2 (struct Machine *chip8 )
{
	if(chip8->sp < 16)
	{
		chip8->sp++;
		chip8->stack[chip8->sp] = chip8->pc;
		chip8->pc = chip8->state.nnn;
	}
}

void p3 (struct Machine *chip8 )
{
	if(chip8->v[chip8->state.x] == chip8->state.kk)
	{
		chip8->pc = (chip8->pc + 2) & 0xFFF;
	}
}

void p4 (struct Machine *chip8 )
{
	if(chip8->v[chip8->state.x] != chip8->state.kk)
	{
		chip8->pc = (chip8->pc + 2) & 0xFFF;
	}
}

void p5 (struct Machine *chip8 )
{
	if(chip8->v[chip8->state.x] == chip8->v[chip8->state.y])
	{
		chip8->pc = (chip8->pc + 2) & 0xFFF;
	}
}

void p6 (struct Machine *chip8 )
{
	chip8->v[chip8->state.x] = chip8->ram[chip8->state.kk];
}

void p7 (struct Machine *chip8 )
{
	chip8->v[chip8->state.x] += chip8->v[chip8->state.y];
}

void p8 (struct Machine *chip8 )
{
	switch(chip8->state.n)
	{
		case 0x0:
			chip8->v[chip8->state.x] = chip8->v[chip8->state.y];
			break;

		case 0x1:
			chip8->v[chip8->state.x] |= chip8->v[chip8->state.y];
			break;

		case 0x2:
			chip8->v[chip8->state.x] &= chip8->v[chip8->state.y];
			break;

		case 0x3:
			chip8->v[chip8->state.x] ^= chip8->v[chip8->state.y];
			break;

		case 0x4:
			chip8->v[0xF] = (chip8->v[chip8->state.x] > chip8->v[chip8->state.x] + chip8->v[chip8->state.y]);
			chip8->v[chip8->state.x] += chip8->v[chip8->state.y];
			break;

		case 0x5:
			chip8->v[0xF] = (chip8->v[chip8->state.x] > chip8->v[chip8->state.y]);
			chip8->v[chip8->state.x] -= chip8->v[chip8->state.y];
			break;

		case 0x6:
			chip8->v[0xF] = chip8->v[chip8->state.x] & 1;
			chip8->v[chip8->state.x] >>= 1;
			break;

		case 0x7:
			chip8->v[0xF] = (chip8->v[chip8->state.y] > chip8->v[chip8->state.x]);
			chip8->v[chip8->state.x] = chip8->v[chip8->state.y] - chip8->v[chip8->state.x];
			break;

		case 0xE:
			chip8->v[0xF] = ((chip8->v[chip8->state.x] & 0x80) != 0);
			chip8->v[chip8->state.x] >>= 1;
			break;
	}
}

void p9 (struct Machine *chip8 )
{
	if(chip8->v[chip8->state.x] != chip8->v[chip8->state.y])
	{
		chip8->pc = (chip8->pc + 2) & 0xFFF;
	}
}

void pA (struct Machine *chip8 )
{
	chip8->i = chip8->state.nnn;
}

void pB (struct Machine *chip8 )
{
	chip8->pc = chip8->state.nnn + chip8->v[chip8->state.x];
}

void pC (struct Machine *chip8 )
{
	chip8->v[chip8->state.x] = (rand()%256) & chip8->state.kk;
}

void pD (struct Machine *chip8 )
{
	//TODO
	printf("DRW %x, %x, %x\n", chip8->state.x, chip8->state.y, chip8->state.n);
}

void pE (struct Machine *chip8 )
{
	if(0x9E == chip8->state.kk)
	{
		//TODO
		printf("SKP %x\n", chip8->state.x);
	}
	else if(0xA1 == chip8->state.kk)
	{
		//TODO
		printf("SKNP %x\n", chip8->state.x);
	}
}

void pF (struct Machine *chip8 )
{
	switch(chip8->state.kk)
	{
		case 0x07:
			chip8->v[chip8->state.x] = chip8->delay;
			break;

		case 0x0A:
			//TODO
			printf("LD %x, K\n", chip8->state.x);
			break;

		case 0x15:
			chip8->delay = chip8->v[chip8->state.x];
			break;

		case 0x18:
			chip8->v[chip8->state.x] = chip8->sound;
			break;

		case 0x1E:
			chip8->i += chip8->v[chip8->state.x];
			break;

		case 0x29:
			//TODO
			printf("LD F, %x\n", chip8->state.x);
			break;

		case 0x33:
			//TODO
			printf("LD B, %x\n", chip8->state.x);
			break;

		case 0x55:
			//TODO
			printf("LD [I], %x \n", chip8->state.x);
			break;

		case 0x65:
			//TODO
			printf("LD %x, [I] \n", chip8->state.x);
			break;
	}
}

static Instruction_Table instructions[16] =
{
	&p0, &p1, &p2, &p3, &p4, &p5, &p6, &p7, &p8, &p9, &pA, &pB, &pC, &pD, &pE, &pF
};

int main(int argc, const char *argv[])
{
	struct Machine chip8;

	//Iniciamos la máquina
		init(&chip8);

	//Cargar la rom
		printf("LOADERING [%s]\n",argv[1]);
		load_rom(&chip8, argv[1]);

	int power = true;

	do
	{
		//TODO: chip8.pc = chip8.pc & 0xFFF;
		chip8.state.opc = (chip8.ram[chip8.pc] << 8) | chip8.ram[chip8.pc+1];
		chip8.pc += 2; // chip8.pc = (chip8.pc + 2) & 0xFFF ???

		if(MEMSIZE == chip8.pc)
		{
			0x200;
		}

		chip8.state.nnn = chip8.state.opc & 0x0FFF;
		chip8.state.kk = chip8.state.opc & 0x00FF;
		chip8.state.n = chip8.state.opc & 0x000F;
		chip8.state.x = (chip8.state.opc >> 8) & 0x000F;
		chip8.state.y = (chip8.state.opc >> 4) & 0x000F;
		chip8.state.p = chip8.state.opc >> 12;

		instructions[chip8.state.p](&chip8);
	}while(true == power);
	return 0;
}
