/* config.h.  Generated from config.h.in by configure.  */
/* config.h.in.  Generated from configure.ac by autoheader.  */

/* Name of package */
#define PACKAGE "gnu_chip8"

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT "yawin@disroot.org"

/* Define to the full name of this package. */
#define PACKAGE_NAME "GNU_Chip8"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "GNU_Chip8 0.0.1"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "gnu_chip8"

/* Define to the home page for this package. */
#define PACKAGE_URL ""

/* Define to the version of this package. */
#define PACKAGE_VERSION "0.0.1"

/* Version number of package */
#define VERSION "0.0.1"
